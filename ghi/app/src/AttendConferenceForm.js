import React, { useEffect, useState } from 'react';
function AttendConferenceForm() {
    const [conferences, setConferences] = useState([]);
    const [conference, setConference] = useState('');
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [hasSignedUp, setHasSignedUp] = useState(false);
    let spinnerClasses = 'd-flex justify-content-center mb-3';
    let dropdownClasses = 'form-select d-none';
    if (conferences.length > 0) {
        spinnerClasses = 'd-flex justify-content-center mb-3 d-none';
        dropdownClasses = 'form-select';
    }

    let messageClasses = 'alert alert-success d-none mb-0';
    let formClasses = '';
    if (hasSignedUp) {
        messageClasses = 'alert alert-success mb-0';
        formClasses = 'd-none';
    }
    const fetchData = async () => {
        const url = 'http://localhost:8000/api/conferences/';
        const response = await fetch(url);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
    };

    const handleConferenceChange = (evt) => {
        const value = evt.target.value;
        setConference(value);
    };

    const handleNameChange = (evt) => {
        const value = evt.target.value;
        setName(value);
    };

    const handleEmailChange = (evt) => {
        const value = evt.target.value;
        setEmail(value);
    };

    const handleSubmit = async (evt) => {
        evt.preventDefault();
        const data = {};
        data.conference = conference;
        data.name = name;
        data.email = email;
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' },
        };

        const newAttendeeUrl = 'http://localhost:8001/api/attendees/';
        const newAttendeeResponse = await fetch(newAttendeeUrl, fetchOptions);

        if (newAttendeeResponse.ok) {
            setHasSignedUp(true);
            setConference('');
            setName('');
            setEmail('');
        }
    };
    useEffect(() => {
        fetchData();
    }, []);
    return (
        <div className="my-5 container">
            <div className="row">
                <div className="col col-sm-auto">
                    <img
                        width="300"
                        className="bg-white rounded shadow d-block mx-auto mb-4"
                        src="/logo.svg"
                    />
                </div>
                <div className="col">
                    <div className="card shadow">
                        <div className="card-body">
                            <form
                                className={formClasses}
                                id="create-attendee-form"
                                onSubmit={handleSubmit}
                            >
                                <h1 className="card-title">
                                    It's Conference Time!
                                </h1>
                                <p className="mb-3">
                                    Please choose which conference you'd like to
                                    attend.
                                </p>
                                <div
                                    className={spinnerClasses}
                                    id="loading-conference-spinner"
                                >
                                    <div
                                        className="spinner-grow text-secondary"
                                        role="status"
                                    >
                                        <span className="visually-hidden">
                                            Loading...
                                        </span>
                                    </div>
                                </div>
                                <div className="mb-3">
                                    <select
                                        onChange={handleConferenceChange}
                                        name="conference"
                                        id="conference"
                                        className={dropdownClasses}
                                        required
                                        value={conference}
                                    >
                                        <option value="">
                                            Choose a conference
                                        </option>
                                        {conferences.map((conference) => (
                                            <option
                                                key={conference.href}
                                                value={conference.href}
                                            >
                                                {conference.name}
                                            </option>
                                        ))}
                                    </select>
                                </div>
                                <p className="mb-3">
                                    Now, tell us about yourself.
                                </p>
                                <div className="row">
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input
                                                onChange={handleNameChange}
                                                required
                                                placeholder="Your full name"
                                                type="text"
                                                id="name"
                                                name="name"
                                                className="form-control"
                                                value={name}
                                            />
                                            <label htmlFor="name">
                                                Your full name
                                            </label>
                                        </div>
                                    </div>
                                    <div className="col">
                                        <div className="form-floating mb-3">
                                            <input
                                                onChange={handleEmailChange}
                                                required
                                                placeholder="Your email address"
                                                type="email"
                                                id="email"
                                                name="email"
                                                className="form-control"
                                                value={email}
                                            />
                                            <label htmlFor="email">
                                                Your email address
                                            </label>
                                        </div>
                                    </div>
                                </div>
                                <button className="btn btn-lg btn-primary">
                                    I'm going!
                                </button>
                            </form>
                            <div
                                className={messageClasses}
                                id="success-message"
                            >
                                Congratulations! You're all signed up!
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    );
}
export default AttendConferenceForm;
