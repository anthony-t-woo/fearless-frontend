import logo from './logo.svg';
import './App.css';
import Nav from './Nav.js';
import MainPage from './MainPage.js';
import AttendeesList from './AttendeesList.js';
import LocationForm from './LocationForm.js';
import ConferenceForm from './ConferenceForm.js';
import PresentationForm from './PresentationForm.js';
import AttendConferenceForm from './AttendConferenceForm.js';
import { BrowserRouter, Routes, Route } from 'react-router-dom';

function App(props) {
    if (props.attendees === undefined) {
        return null;
    }
    return (
        <BrowserRouter>
            <Nav />
            <div className="container">
                <Routes>
                    <Route index element={<MainPage />} />
                    <Route path="locations">
                        <Route path="new" element={<LocationForm />} />
                    </Route>
                    <Route path="presentations">
                        <Route path="new" element={<PresentationForm />} />
                    </Route>
                    <Route path="conferences">
                        <Route path="new" element={<ConferenceForm />} />
                    </Route>
                    <Route path="attendees">
                        <Route
                            index
                            element={
                                <AttendeesList attendees={props.attendees} />
                            }
                        />
                        <Route path="new" element={<AttendConferenceForm />} />
                    </Route>
                </Routes>
            </div>
        </BrowserRouter>
    );
}

export default App;
