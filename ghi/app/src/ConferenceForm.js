import React, { useEffect, useState } from 'react';
function ConferenceForm() {
    const [locations, setLocations] = useState([]);
    const [endDate, setEndDate] = useState('');
    const [startDate, setStartDate] = useState('');
    const [description, setDescription] = useState('');
    const [maxPresentations, setMaxPresentations] = useState('');
    const [maxAttendees, setMaxAttendees] = useState('');
    const [name, setName] = useState('');
    const [location, setLocation] = useState('');
    const fetchData = async () => {
        const locationUrl = 'http://localhost:8000/api/locations/';

        const response = await fetch(locationUrl);
        if (response.ok) {
            const data = await response.json();
            setLocations(data.locations);
        }
    };

    const handleNameChange = (evt) => {
        const value = evt.target.value;
        setName(value);
    };

    const handleStartDateChange = (evt) => {
        const value = evt.target.value;
        setStartDate(value);
    };

    const handleEndDateChange = (evt) => {
        const value = evt.target.value;
        setEndDate(value);
    };

    const handleDescriptionChange = (evt) => {
        const value = evt.target.value;
        setDescription(value);
    };

    const handleMaxPresentationsChange = (evt) => {
        const value = evt.target.value;
        setMaxPresentations(value);
    };

    const handleMaxAttendeesChange = (evt) => {
        const value = evt.target.value;
        setMaxAttendees(value);
    };

    const handleLocationChange = (evt) => {
        const value = evt.target.value;
        setLocation(value);
    };

    const handleSubmit = async (evt) => {
        evt.preventDefault();
        const data = {};
        data.name = name;
        data.starts = startDate;
        data.ends = endDate;
        data.description = description;
        data.max_attendees = maxAttendees;
        data.max_presentations = maxPresentations;
        data.location = location;
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' },
        };
        const newConferenceUrl = 'http://localhost:8000/api/conferences/';
        const newConferenceResponse = await fetch(
            newConferenceUrl,
            fetchOptions
        );
        if (newConferenceResponse.ok) {
            const newConference = await newConferenceResponse.json();
            console.log(newConference);
            setName('');
            setStartDate('');
            setEndDate('');
            setDescription('');
            setMaxAttendees('');
            setMaxPresentations('');
            setLocation('');
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new conference</h1>
                    <form id="create-conference-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                value={name}
                                onChange={handleNameChange}
                                placeholder="Name"
                                required
                                name="name"
                                type="text"
                                id="name"
                                className="form-control"
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleStartDateChange}
                                placeholder="mm/dd/yyyy"
                                required
                                name="starts"
                                type="date"
                                id="starts"
                                className="form-control"
                                value={startDate}
                            />
                            <label htmlFor="starts">Start date</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleEndDateChange}
                                placeholder="mm/dd/yyyy"
                                required
                                name="ends"
                                type="date"
                                id="ends"
                                className="form-control"
                                value={endDate}
                            />
                            <label htmlFor="ends">End date</label>
                        </div>
                        <div className="text-area mb-3">
                            <textarea
                                onChange={handleDescriptionChange}
                                required
                                name="description"
                                type="text"
                                id="description"
                                className="form-control"
                                value={description}
                            ></textarea>
                            <label htmlFor="description">Description</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleMaxPresentationsChange}
                                placeholder="Maximum presentations"
                                required
                                name="max_presentations"
                                type="number"
                                id="max_presentations"
                                className="form-control"
                                value={maxPresentations}
                            />
                            <label htmlFor="max_presentations">
                                Maximum presentations
                            </label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleMaxAttendeesChange}
                                placeholder="Maximum attendees"
                                required
                                name="max_attendees"
                                type="number"
                                id="max_attendees"
                                className="form-control"
                                value={maxAttendees}
                            />
                            <label htmlFor="max_attendees">
                                Maximum attendees
                            </label>
                        </div>
                        <div className="mb-3">
                            <select
                                onChange={handleLocationChange}
                                required
                                name="location"
                                id="location"
                                className="form-select"
                                value={location}
                            >
                                <option value="">Choose a location</option>
                                {locations.map((location) => (
                                    <option
                                        key={location.id}
                                        value={location.id}
                                    >
                                        {location.name}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default ConferenceForm;
