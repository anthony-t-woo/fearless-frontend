import React, { useEffect, useState } from 'react';
function PresentationForm() {
    const [conferences, setConferences] = useState([]);
    const [name, setName] = useState('');
    const [email, setEmail] = useState('');
    const [company, setCompany] = useState('');
    const [title, setTitle] = useState('');
    const [synopsis, setSynopsis] = useState('');
    const [conference, setConference] = useState('');

    const fetchData = async () => {
        const conferenceUrl = 'http://localhost:8000/api/conferences/';
        const response = await fetch(conferenceUrl);
        if (response.ok) {
            const data = await response.json();
            setConferences(data.conferences);
        }
    };

    const handleNameChange = (evt) => {
        const value = evt.target.value;
        setName(value);
    };
    const handleEmailChange = (evt) => {
        const value = evt.target.value;
        setEmail(value);
    };
    const handleCompanyChange = (evt) => {
        const value = evt.target.value;
        setCompany(value);
    };
    const handleTitleChange = (evt) => {
        const value = evt.target.value;
        setTitle(value);
    };
    const handleSynopsisChange = (evt) => {
        const value = evt.target.value;
        setSynopsis(value);
    };
    const handleConferenceChange = (evt) => {
        const value = evt.target.value;
        setConference(value);
    };

    const handleSubmit = async (evt) => {
        evt.preventDefault();
        const data = {};
        data.presenter_name = name;
        data.presenter_email = email;
        data.company_name = company;
        data.title = title;
        data.synopsis = synopsis;
        data.conference = conference;

        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' },
        };
        // const select = document.getElementById('conference');
        // const conferenceHref = select.options[select.selectedIndex].value;
        const newPresentationUrl = `http://localhost:8000${data.conference}presentations/`;

        const newPresentationResponse = await fetch(
            newPresentationUrl,
            fetchOptions
        );
        if (newPresentationResponse.ok) {
            const newPresentation = await newPresentationResponse.json();

            setName('');
            setEmail('');
            setCompany('');
            setTitle('');
            setSynopsis('');
            setConference('');
        }
    };
    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new presentation</h1>
                    <form id="create-presentation-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleNameChange}
                                placeholder="Presenter name"
                                required
                                name="presenter_name"
                                type="text"
                                id="presenter_name"
                                className="form-control"
                                value={name}
                            />
                            <label htmlFor="presenter_name">
                                Presenter name
                            </label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleEmailChange}
                                placeholder="email"
                                required
                                name="presenter_email"
                                type="email"
                                id="presenter_email"
                                className="form-control"
                                value={email}
                            />
                            <label htmlFor="presenter_email">
                                Presenter email
                            </label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleCompanyChange}
                                placeholder="Company name"
                                required
                                name="company_name"
                                type="text"
                                id="company_name"
                                className="form-control"
                                value={company}
                            />
                            <label htmlFor="company_name">Company name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleTitleChange}
                                placeholder="Title"
                                required
                                name="title"
                                type="text"
                                id="title"
                                className="form-control"
                                value={title}
                            />
                            <label htmlFor="title">Title</label>
                        </div>
                        <div className="text-area mb-3">
                            <textarea
                                onChange={handleSynopsisChange}
                                required
                                name="synopsis"
                                type="text"
                                id="synopsis"
                                className="form-control"
                                value={synopsis}
                            ></textarea>
                            <label htmlFor="synopsis">Synopsis</label>
                        </div>
                        <div className="mb-3">
                            <select
                                onChange={handleConferenceChange}
                                required
                                name="conference"
                                id="conference"
                                className="form-select"
                                value={conference}
                            >
                                <option value="">Choose a conference</option>
                                {conferences.map((conference) => (
                                    <option
                                        value={conference.href}
                                        key={conference.href}
                                    >
                                        {conference.name}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}
export default PresentationForm;
