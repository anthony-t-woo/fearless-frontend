import React, { useEffect, useState } from 'react';
function LocationForm() {
    const [states, setStates] = useState([]);
    const [roomCount, setRoomCount] = useState('');
    const [city, setCity] = useState('');
    const [name, setName] = useState('');
    const [state, setState] = useState('');
    const fetchData = async () => {
        const stateUrl = 'http://localhost:8000/api/states/';
        const response = await fetch(stateUrl);
        if (response.ok) {
            const data = await response.json();
            setStates(data.states);
        }
    };

    const handleNameChange = (evt) => {
        const value = evt.target.value;
        setName(value);
    };

    const handleRoomCountChange = (evt) => {
        const value = evt.target.value;
        setRoomCount(value);
    };

    const handleCityChange = (evt) => {
        const value = evt.target.value;
        setCity(value);
    };

    const handleStateChange = (evt) => {
        const value = evt.target.value;
        setState(value);
    };
    const handleSubmit = async (evt) => {
        evt.preventDefault();
        const data = {};
        data.room_count = roomCount;
        data.state = state;
        data.name = name;
        data.city = city;
        const fetchOptions = {
            method: 'post',
            body: JSON.stringify(data),
            headers: { 'Content-Type': 'application/json' },
        };
        const newLocationUrl = 'http://localhost:8000/api/locations/';
        const newLocationResponse = await fetch(newLocationUrl, fetchOptions);
        if (newLocationResponse.ok) {
            const newLocation = await newLocationResponse.json();
            console.log(newLocation);
            setName('');
            setRoomCount('');
            setCity('');
            setState('');
        }
    };

    useEffect(() => {
        fetchData();
    }, []);

    return (
        <div className="row">
            <div className="offset-3 col-6">
                <div className="shadow p-4 mt-4">
                    <h1>Create a new location</h1>
                    <form id="create-location-form" onSubmit={handleSubmit}>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleNameChange}
                                placeholder="Name"
                                required
                                name="name"
                                type="text"
                                id="name"
                                className="form-control"
                                value={name}
                            />
                            <label htmlFor="name">Name</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleRoomCountChange}
                                placeholder="Room count"
                                required
                                name="room_count"
                                type="number"
                                id="room_count"
                                className="form-control"
                                value={roomCount}
                            />
                            <label htmlFor="room_count">Room count</label>
                        </div>
                        <div className="form-floating mb-3">
                            <input
                                onChange={handleCityChange}
                                placeholder="City"
                                required
                                name="city"
                                type="text"
                                id="city"
                                className="form-control"
                                value={city}
                            />
                            <label htmlFor="city">City</label>
                        </div>
                        <div className="mb-3">
                            <select
                                onChange={handleStateChange}
                                required
                                name="state"
                                id="state"
                                className="form-select"
                                value={state}
                            >
                                <option key="prompt" value="">
                                    Choose a state
                                </option>
                                {states.map((state) => (
                                    <option
                                        key={state.abbreviation + state.name}
                                        value={state.abbreviation}
                                    >
                                        {state.name}
                                    </option>
                                ))}
                            </select>
                        </div>
                        <button className="btn btn-primary">Create</button>
                    </form>
                </div>
            </div>
        </div>
    );
}

export default LocationForm;
